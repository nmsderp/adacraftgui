import PropTypes from 'prop-types';
import React from 'react';
import bindAll from 'lodash.bindall';
import QuestionComponent from '../components/question/question.jsx';
import adacraft from '../adacraft/index.js';

class Question extends React.Component {
    constructor (props) {
        super(props);
        bindAll(this, [
            'handleChange',
            'handleKeyPress',
            'handleSubmit'
        ]);
        this.state = {
            answer: '',
            // The use of a file input will open a file dialog that will exit
            // fullscreen. So on creation of the question we check if fullscreen
            // is on and will restore it when the answer is submited.
            fullscreenWasActiveOnStart: document.fullscreenElement !== null
        };
    }
    isFileQuestion () {
        return this.props.questionType.includes('file');
    }
    handleChange (event) {
        if (this.isFileQuestion()) {
            const files = event.target.files
            const file = files[0]
            var reader = new FileReader();
            reader.onload = (event) => {
                this.setState({answer: event.target.result});
            }
            if (this.props.questionType === 'file as text') {
                reader.readAsText(file)
            } else if (this.props.questionType === 'file as data URL') {
                reader.readAsDataURL(file)
            }
        } else {
            this.setState({answer: event.target.value});
        }
    }
    handleKeyPress (event) {
        if (event.key === 'Enter') this.handleSubmit();
    }
    handleSubmit () {
        if (this.isFileQuestion()) {
            // Restore fullscreen if it was on when the file dialog was openned.
            if (this.state.fullscreenWasActiveOnStart) {
                adacraft.requestBrowserFullscreen();
            }
        }
        this.props.onQuestionAnswered(this.state.answer);
    }
    render () {
        return (
            <QuestionComponent
                answer={this.state.answer}
                question={this.props.question}
                questionType={this.props.questionType}
                onChange={this.handleChange}
                onClick={this.handleSubmit}
                onKeyPress={this.handleKeyPress}
            />
        );
    }
}

Question.propTypes = {
    onQuestionAnswered: PropTypes.func.isRequired,
    question: PropTypes.string,
    questionType: PropTypes.string.isRequired
};

export default Question;
