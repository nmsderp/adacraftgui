const assetId = 'd377145cb8ffa483151bc7a47f0b4662'
import assetContent from '!raw-loader!./d377145cb8ffa483151bc7a47f0b4662.svg';

export default {
    blocks: {},
    costume: {
        assetContent,
        assetType: 'ImageVector',
        projectData: {
            assetId,
            name: 'star',
            bitmapResolution: 1,
            md5ext: `${assetId}.svg`,
            dataFormat: 'svg',
            rotationCenterX: 24,
            rotationCenterY: 24
        }
    }
};