import stage from './gui/stage'
import stageWrapper from './gui/stage-wrapper'
import defaultProject from './default-project';

export default {
    gui: {
        stage,
        stageWrapper
    },
    defaultProject
};