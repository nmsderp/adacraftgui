import styles from './stop-all.css';
import icon from './icon--stop-all.svg'

export default {
    styles,
    icon
}