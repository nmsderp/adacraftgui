import styles from './stage-header.css';
import fullscreen from './icon--fullscreen.svg'
import unfullscreen from './icon--unfullscreen.svg'

export default {
    styles,
    icons: {
        fullscreen,
        unfullscreen
    }
}