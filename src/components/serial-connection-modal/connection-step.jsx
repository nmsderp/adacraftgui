import { defineMessages, FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

import Box from '../box/box.jsx';
import checkIcon from './icons/check.png';
import crossIcon from './icons/cross.png';
import infosIcon from './icons/infos.png';
import styles from './serial-connection-modal.css';

const ConnectionStep = props => {
    let messages;
    switch (props.title) {
        case 'vittaarduino':
            messages = defineMessages({
                connectBoard: {
                    defaultMessage: "Connect your arduino to the browser",
                    description: "Text shown while connecting board",
                    id: "gui.connection.serial.connectBoard.arduino"
                },
                downloadingFile: {
                    defaultMessage: "To interact with arduino, please upload the following program into your arduino",
                    description: "Text shown while uploading board",
                    id: "gui.connection.serial.downloadingFile.arduino"
                },
                downloadPopupIntro: {
                    defaultMessage: "To transfer the program into your arduino, follow these steps:",
                    description: "Text shown to help user when clicking on download popup icon - intro",
                    id: "gui.connection.serial.downloadPopup.intro.arduino"
                },
                downloadPopupStep1: {
                    defaultMessage: "1. Connect your arduino to your computer with a USB cable. Your computer should detect it as a new USB device.",
                    description: "Text shown to help user when clicking on download popup icon - step1",
                    id: "gui.connection.serial.downloadPopup.step1.arduino"
                },
                downloadPopupStep2: {
                    defaultMessage: "2. Click on \"Upload program\" to upload the binary file.",
                    description: "Text shown to help user when clicking on download popup icon - step2",
                    id: "gui.connection.serial.downloadPopup.step2.arduino"
                },
                downloadPopupStep3: {
                    defaultMessage: "3. Select your Arduino UNO board in the peripherals, and click on connect. Wait few seconds.",
                    description: "Text shown to help user when clicking on download popup icon - step3",
                    id: "gui.connection.serial.downloadPopup.step3.arduino"
                },
                downloadFileButton: {
                    defaultMessage: "Upload program",
                    description: "Button in prompt for downloading file",
                    id: "gui.connection.serial.downloadFileButton.arduino"
                },
                connectPopup: {
                    defaultMessage: "If you don't see your arduino in the list, check that it is correctly plugged by USB  and try again. If you are using your arduino on other interfaces, try to disconnect them and try again.",
                    description: "Text shown to help user when clicking on connect popup icon",
                    id: "gui.connection.serial.connectPopup.arduino"
                },
                connectButton: {
                    defaultMessage: "Connect",
                    description: "Text shown on connect button",
                    id: "gui.connection.serial.connectButton.arduino"
                }
            });
            break;
        case 'vittamicrobit':
            messages = defineMessages({
                connectBoard: {
                    defaultMessage: "Connect your micro:bit to the browser",
                    description: "Text shown while connecting board",
                    id: "gui.connection.serial.connectBoard.microbit"
                },
                downloadingFile: {
                    defaultMessage: "To interact with micro:bit, please download the following program and transfer it into your micro:bit",
                    description: "Text shown while downloading board",
                    id: "gui.connection.serial.downloadingFile.microbit"
                },
                downloadPopupIntro: {
                    defaultMessage: "To transfer the program into your micro:bit, follow these steps:",
                    description: "Text shown to help user when clicking on download popup icon - intro",
                    id: "gui.connection.serial.downloadPopup.intro.microbit"
                },
                downloadPopupStep1: {
                    defaultMessage: "1. Connect your micro:bit to your computer with a USB cable. Your computer should detect it as a new USB device called MICROBIT.",
                    description: "Text shown to help user when clicking on download popup icon - step1",
                    id: "gui.connection.serial.downloadPopup.step1.microbit"
                },
                downloadPopupStep2: {
                    defaultMessage: "2. Click on Download .hex to download the binary file and locate it on your computer.",
                    description: "Text shown to help user when clicking on download popup icon - step2",
                    id: "gui.connection.serial.downloadPopup.step2.microbit"
                },
                downloadPopupStep3: {
                    defaultMessage: "3. Move the .hex file from your computer onto the MICROBIT drive.",
                    description: "Text shown to help user when clicking on download popup icon - step3",
                    id: "gui.connection.serial.downloadPopup.step3.microbit"
                },
                downloadFileButton: {
                    defaultMessage: "Download .hex",
                    description: "Button in prompt for downloading file",
                    id: "gui.connection.serial.downloadFileButton.microbit"
                },
                connectPopup: {
                    defaultMessage: "If you don't see your micro:bit in the list, check that it is correctly plugged by USB  and try again. If you are using your micro:bit on other interfaces, try to disconnect them and try again.",
                    description: "Text shown to help user when clicking on connect popup icon",
                    id: "gui.connection.serial.connectPopup.microbit"
                },
                connectButton: {
                    defaultMessage: "Connect",
                    description: "Text shown on connect button",
                    id: "gui.connection.serial.connectButton.microbit"
                }
            });
            break;
        case 'vittaSTM32':
            messages = defineMessages({
                connectBoard: {
                    defaultMessage: "Connect your STM32 to the browser",
                    description: "Text shown while connecting board",
                    id: "gui.connection.serial.connectBoard.stm32"
                },
                downloadingFile: {
                    defaultMessage: "To interact with STM32, please download the following program and transfer it into your STM32",
                    description: "Text shown while downloading board",
                    id: "gui.connection.serial.downloadingFile.stm32"
                },
                downloadPopupIntro: {
                    defaultMessage: "To transfer the program into your STM32, follow these steps:",
                    description: "Text shown to help user when clicking on download popup icon - intro",
                    id: "gui.connection.serial.downloadPopup.intro.stm32"
                },
                downloadPopupStep1: {
                    defaultMessage: "1. Connect your STM32 to your computer with a USB cable. Your computer should detect it as a new USB device called PYBFLASH.",
                    description: "Text shown to help user when clicking on download popup icon - step1",
                    id: "gui.connection.serial.downloadPopup.step1.stm32"
                },
                downloadPopupStep2: {
                    defaultMessage: "2. Click on Download .py to download the python file and locate it on your computer.",
                    description: "Text shown to help user when clicking on download popup icon - step2",
                    id: "gui.connection.serial.downloadPopup.step2.stm32"
                },
                downloadPopupStep3: {
                    defaultMessage: "3. Move the .py file from your computer onto the PYBFLASH drive.",
                    description: "Text shown to help user when clicking on download popup icon - step3",
                    id: "gui.connection.serial.downloadPopup.step3.stm32"
                },
                downloadFileButton: {
                    defaultMessage: "Download .py",
                    description: "Button in prompt for downloading file",
                    id: "gui.connection.serial.downloadFileButton.stm32"
                },
                connectPopup: {
                    defaultMessage: "If you don't see your STM32 in the list, check that it is correctly plugged by USB  and try again. If you are using your STM32 on other interfaces, try to disconnect them and try again.",
                    description: "Text shown to help user when clicking on connect popup icon",
                    id: "gui.connection.serial.connectPopup.stm32"
                },
                connectButton: {
                    defaultMessage: "Connect",
                    description: "Text shown on connect button",
                    id: "gui.connection.serial.connectButton.stm32"
                }
            });
            break;
        case 'vittaESP32':
            messages = {
                connectBoard: {
                    defaultMessage: "Connect your ESP32 to the browser",
                    description: "Text shown while connecting board",
                    id: "gui.connection.serial.connectBoard.esp32"
                },
                downloadingFile: {
                    defaultMessage: "To interact with ESP32, please upload the following program into your board",
                    description: "Text shown while connecting board",
                    id: "gui.connection.serial.downloadingFile.esp32"
                },
                downloadPopupIntro: {
                    defaultMessage: "To transfer the program into your ESP32, follow these steps:",
                    description: "Text shown to help user when clicking on download popup icon - intro",
                    id: "gui.connection.serial.downloadPopup.intro.esp32"
                },
                downloadPopupStep1: {
                    defaultMessage: "1. Connect your ESP32 to your computer with a USB cable.",
                    description: "Text shown to help user when clicking on download popup icon - step1",
                    id: "gui.connection.serial.downloadPopup.step1.esp32"
                },
                downloadPopupStep2: {
                    defaultMessage: "2. Click on Upload button. Select the board in the peripherals.",
                    description: "Text shown to help user when clicking on download popup icon - step2",
                    id: "gui.connection.serial.downloadPopup.step2.esp32"
                },
                downloadPopupStep3: {
                    defaultMessage: "3. Click on Connexion at the bottom of connection window. Wait about 10 seconds.",
                    description: "Text shown to help user when clicking on download popup icon - step3",
                    id: "gui.connection.serial.downloadPopup.step3.esp32"
                },
                downloadFileButton: {
                    defaultMessage: "Upload",
                    description: "Button in prompt for downloading file",
                    id: "gui.connection.serial.downloadFileButton.esp32"
                },
                connectPopup: {
                    defaultMessage: "If you don't see your ESP32 in the list, check that it is correctly plugged by USB  and try again. If you are using your ESP32 on other interfaces, try to disconnect them and try again.",
                    description: "Text shown to help user when clicking on connect popup icon",
                    id: "gui.connection.serial.connectPopup.esp32"
                },
                connectButton: {
                    defaultMessage: "Close",
                    description: "Button in prompt for connecting the board and closeing the modal",
                    id: "gui.connection.serial.connectButton.esp32"
                }
            };
            break;
    }
    return (
        <Box className={styles.body}>
            {navigator.serial ? (
                <div>
                    <Box className={styles.activityArea}>
                        <div className={styles.activityAreaInfo}>
                            <div className={styles.centeredRow}>
                                <img
                                    className={styles.checkSerialIcon}
                                    src={checkIcon}
                                />
                                <FormattedMessage
                                    defaultMessage="Your browser is compatible with this extension"
                                    description="Text shown if navigator does not support Serial API"
                                    id="gui.connection.serial.hasSerialAPI"
                                />
                                <div className={styles.serialpopup}>
                                    <img
                                        className={styles.infosSerialIcon}
                                        src={infosIcon}
                                        onClick={props.onSerialPopupOpening}
                                    />
                                    <span className={styles.serialpopuptext} id="serialPopup">
                                        <FormattedMessage
                                            defaultMessage="Vittascience's board extension uses Serial API technology to communicate between the browser and the micropython board."
                                            description="Text shown to help user when clicking on serial popup icon"
                                            id="gui.connection.serial.serialPopup"
                                        />
                                        <br></br>
                                        <a target="_blank" delay="100" href="https://developer.mozilla.org/en-US/docs/Web/API/Serial#browser_compatibility">
                                            <FormattedMessage
                                                defaultMessage="See the list of compatible browsers."
                                                description="Text shown to redirect user to serial API compatible browsers page"
                                                id="gui.connection.serial.browsersList"
                                            />
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </Box>
                    <Box className={styles.bottomArea}>
                        <div>
                            <FormattedMessage {...messages.downloadingFile} ></FormattedMessage>
                            <div className={styles.downloadpopup}>
                                <img
                                    className={styles.infosSerialIcon}
                                    src={infosIcon}
                                    onClick={props.onDownloadPopupOpening}
                                />
                                <span className={styles.downloadpopuptext} id="downloadPopup">
                                    <FormattedMessage {...messages.downloadPopupIntro} />
                                    <br></br><br></br>
                                    <FormattedMessage {...messages.downloadPopupStep1} />
                                    <br></br>
                                    <FormattedMessage {...messages.downloadPopupStep2} />
                                    <br></br>
                                    <FormattedMessage {...messages.downloadPopupStep3} />
                                    <br></br>
                                </span>
                            </div>
                        </div>
                        <div className={classNames(styles.centeredRow, styles.marginBottom)}>
                            <button
                                className={styles.connectionButton}
                                onClick={props.onDownloading}
                            >
                                <FormattedMessage {...messages.downloadFileButton} />
                            </button>
                        </div>
                        <div className={classNames(styles.centeredRow, styles.marginBottom)}>
                            <FormattedMessage
                                defaultMessage="To see and modify this program on Vittascience, "
                                description="Text shown for redirecting to python source code"
                                id="gui.connection.serial.checkSourceCode"
                            />
                            <a target="_blank" delay="100" href={props.projectLink}>
                                <FormattedMessage
                                    defaultMessage="click here"
                                    description="Text shown click here button"
                                    id="gui.connection.serial.clickHere"
                                />
                            </a>
                        </div>
                    </Box>
                    <Box className={styles.activityAreaBis}>
                        <FormattedMessage {...messages.connectBoard} />
                        <div className={styles.connectpopup}>
                            <img
                                className={styles.infosSerialIcon}
                                src={infosIcon}
                                onClick={props.onConnectPopupOpening}
                            />
                            <span className={styles.connectpopuptext} id="connectPopup">
                                <FormattedMessage {...messages.connectPopup} />
                            </span>
                        </div>
                    </Box>
                    <Box className={styles.activityAreaBis}>
                        <div className={styles.centeredRow}>
                            <button
                                className={classNames(styles.connectionButton)}
                                onClick={props.onConnecting}
                            >
                                <FormattedMessage
                                    defaultMessage="Connect"
                                    description="Button in prompt for connecting a board"
                                    id="gui.connection.serial.connectButton"
                                />
                            </button>
                        </div>
                    </Box>
                </div>
            ) : (
                <Box className={styles.activityArea}>
                    <div className={styles.activityAreaInfo}>
                        <div className={styles.centeredRow}>
                            <img
                                className={styles.crossSerialIcon}
                                src={crossIcon}
                            />
                            <FormattedMessage
                                defaultMessage="Your browser does not support Serial API"
                                description="Text shown if navigator does not support Serial API"
                                id="gui.connection.serial.noSerialAPI"
                            />
                            <div className={styles.serialpopup}>
                                <img
                                    className={styles.infosSerialIcon}
                                    src={infosIcon}
                                    onClick={props.onSerialPopupOpening}
                                />
                                <span className={styles.serialpopuptext} id="serialPopup">
                                    <FormattedMessage
                                        defaultMessage="Vittascience's board extension uses Serial API technology to communicate between the browser and the micropython board."
                                        description="Text shown to help user when clicking on serial popup icon"
                                        id="gui.connection.serial.serialPopup"
                                    />
                                    <br></br>
                                    <a target="_blank" delay="100" href="https://developer.mozilla.org/en-US/docs/Web/API/Serial#browser_compatibility">
                                        <FormattedMessage
                                            defaultMessage="See the list of compatible browsers."
                                            description="Text shown to redirect user to serial API compatible browsers page"
                                            id="gui.connection.serial.browsersList"
                                        />
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </Box>
            )
            }
        </Box>
    )
};

ConnectionStep.propTypes = {
    onConnecting: PropTypes.func,
    onDownloading: PropTypes.func,
};

export default ConnectionStep;
