import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import adacraft from '../../adacraft';

import defaultIcon from './icon--stop-all.svg';
import defaultStyles from './stop-all.css';
const defaultConfig = {
    icon: defaultIcon,
    styles: defaultStyles
}

// [adacraft] The layout can be configured based on the running environment.
let config = adacraft.features.config.gui.stopAll || defaultConfig;

const StopAllComponent = function (props) {
    const {
        active,
        className,
        onClick,
        title,
        ...componentProps
    } = props;
    return (
        <img
            className={classNames(
                className,
                config.styles.stopAll,
                {
                    [config.styles.isActive]: active
                }
            )}
            draggable={false}
            src={config.icon}
            title={title}
            onClick={onClick}
            {...componentProps}
        />
    );
};

StopAllComponent.propTypes = {
    active: PropTypes.bool,
    className: PropTypes.string,
    onClick: PropTypes.func.isRequired,
    title: PropTypes.string
};

StopAllComponent.defaultProps = {
    active: false,
    title: 'Stop'
};

export default StopAllComponent;
