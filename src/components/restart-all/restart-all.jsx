import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import redoIcon from './icon--redo.svg';
import styles from './restart-all.css';

const RestartAllComponent = function (props) {
    const {
        active,
        className,
        onClick,
        title,
        ...componentProps
    } = props;
    return (
        <img
            className={classNames(
                className,
                styles.restartAll,
                {
                    [styles.isActive]: active
                }
            )}
            draggable={false}
            src={redoIcon}
            title={title}
            onClick={onClick}
            {...componentProps}
        />
    );
};
RestartAllComponent.propTypes = {
    active: PropTypes.bool,
    className: PropTypes.string,
    onClick: PropTypes.func.isRequired,
    title: PropTypes.string
};
RestartAllComponent.defaultProps = {
    active: false,
    title: 'Restart'
};
export default RestartAllComponent;
