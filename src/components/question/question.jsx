import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import styles from './question.css';
import Input from '../forms/input.jsx';
import FileInput from '../forms/file-input.jsx';
import enterIcon from './icon--enter.svg';

const QuestionComponent = props => {
    const {
        answer,
        className,
        question,
        questionType,
        onChange,
        onClick,
        onKeyPress
    } = props;
    const isFileQuestion = questionType.includes('file')
    return (
        <div className={className}>
            <div className={styles.questionContainer}>
                {question ? (
                    <div className={styles.questionLabel}>{question}</div>
                ) : null}
                <div className={styles.questionInput}>
                    {isFileQuestion ? (
                        <FileInput onChange={onChange}/>
                    ) : (
                        <Input
                            autoFocus
                            value={answer}
                            onChange={onChange}
                            onKeyPress={onKeyPress}
                        />
                    )}
                    <button
                        className={classNames(
                            styles.questionSubmitButton,
                            { [styles.fileQuestion]: isFileQuestion }
                        )}
                        onClick={onClick}
                    >
                        <img
                            className={styles.questionSubmitButtonIcon}
                            draggable={false}
                            src={enterIcon}
                        />
                    </button>
                </div>
            </div>
        </div>
    );
};

QuestionComponent.propTypes = {
    answer: PropTypes.string,
    className: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    onClick: PropTypes.func.isRequired,
    onKeyPress: PropTypes.func.isRequired,
    question: PropTypes.string,
    questionType: PropTypes.string.isRequired
};

export default QuestionComponent;
