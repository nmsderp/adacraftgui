import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import VM from 'scratch-vm';

import Box from '../box/box.jsx';
import {STAGE_DISPLAY_SIZES} from '../../lib/layout-constants.js';
import StageHeader from '../../containers/stage-header.jsx';
import Stage from '../../containers/stage.jsx';
import Loader from '../loader/loader.jsx';

import defaultStyles from './stage-wrapper.css';

const defaultConfig = {
    styles: defaultStyles
}

import adacraft from '../../adacraft/index.js';
// [adacraft] The layout can be configured based on the running environment.
let config = adacraft.features.config.gui.stageWrapper || defaultConfig;

const { styles } = config

const StageWrapperComponent = function (props) {
    const {
        isEmbedded,
        isFullScreen,
        isRtl,
        isRendererSupported,
        loading,
        stageSize,
        vm
    } = props;

    // TODO remove coupling with adacraft.
    const headerIsAbove = adacraft.features.decisions.stageHeaderIsAbove;
    const headerIsBelow = !headerIsAbove;
    const stageHeader = (
        <Box className={styles.stageMenuWrapper}>
            <StageHeader
                stageSize={stageSize}
                vm={vm}
            />
        </Box>
    );

    return (
        <Box
            className={classNames(
                styles.stageWrapper,
                {[styles.embedded]: isEmbedded},
                {[styles.fullScreen]: isFullScreen},
            )}
            dir={isRtl ? 'rtl' : 'ltr'}
        >
            { headerIsAbove ? stageHeader : null }
            <Box className={styles.stageCanvasWrapper}>
                {
                    isRendererSupported ?
                        <Stage
                            stageSize={stageSize}
                            vm={vm}
                        /> :
                        null
                }
            </Box>
            { headerIsBelow ? stageHeader : null }
            {loading ? (
                <Loader isFullScreen={isFullScreen} />
            ) : null}
        </Box>
    );
};

StageWrapperComponent.propTypes = {
    isEmbedded: PropTypes.bool,
    isFullScreen: PropTypes.bool,
    isRendererSupported: PropTypes.bool.isRequired,
    isRtl: PropTypes.bool.isRequired,
    loading: PropTypes.bool,
    stageSize: PropTypes.oneOf(Object.keys(STAGE_DISPLAY_SIZES)).isRequired,
    vm: PropTypes.instanceOf(VM).isRequired
};

export default StageWrapperComponent;
