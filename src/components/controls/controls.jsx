import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';
import {defineMessages, injectIntl, intlShape} from 'react-intl';

import GreenFlag from '../green-flag/green-flag.jsx';
import StopAll from '../stop-all/stop-all.jsx';

// [adacraft] The "restart all" button is introduced for Vittascience
// environment.
import RestartAll from '../restart-all/restart-all.jsx';

import TurboMode from '../turbo-mode/turbo-mode.jsx';
import FramerateIndicator from '../tw-framerate-indicator/framerate-indicator.jsx';

import adacraft from '../../adacraft/index.js';
const displayRestartAllButton = adacraft.features.decisions.displayRestartAllButton;
const showExtraInfo = adacraft.features.decisions.showExtraInfo;
const allowGreenFlagWhileActive = adacraft.features.decisions.allowGreenFlagWhileActive;

import styles from './controls.css';

const messages = defineMessages({
    goTitle: {
        id: 'gui.controls.go',
        defaultMessage: 'Go',
        description: 'Green flag button title'
    },
    stopTitle: {
        id: 'gui.controls.stop',
        defaultMessage: 'Stop',
        description: 'Stop button title'
    },
    restartTitle: {
        id: 'gui.controls.restart',
        defaultMessage: 'Restart',
        description: 'Restart button title'
    }
});

const Controls = function (props) {
    const {
        active,
        className,
        intl,
        onGreenFlagClick,
        onStopAllClick,
        turbo,
        framerate,
        interpolation,
        ...componentProps
    } = props;
    return (
        <div
            className={classNames(styles.controlsContainer, className)}
            {...componentProps}
        >
            <GreenFlag
                active={active}
                title={intl.formatMessage(messages.goTitle)}
                onClick={
                    // [adacraft] For some envs (like Vittascience) we want to
                    // disable green flag action when the project is running. 
                    (!active || allowGreenFlagWhileActive)
                    ? onGreenFlagClick
                    : () => {}
                }
            />
            <StopAll
                active={active}
                title={intl.formatMessage(messages.stopTitle)}
                onClick={onStopAllClick}
            />
            {displayRestartAllButton ? (
                <RestartAll
                    active={active}
                    title={intl.formatMessage(messages.restartTitle)}
                    onClick={active ? onGreenFlagClick : () => {}}
                />
            ): null}
            {(turbo && showExtraInfo) ? (
                <TurboMode />
            ) : null}
            {showExtraInfo ? (                
                <FramerateIndicator
                    framerate={framerate}
                    interpolation={interpolation}
                />
            ) : null}
        </div>
    );
};

Controls.propTypes = {
    active: PropTypes.bool,
    className: PropTypes.string,
    intl: intlShape.isRequired,
    onGreenFlagClick: PropTypes.func.isRequired,
    onStopAllClick: PropTypes.func.isRequired,
    framerate: PropTypes.number,
    interpolation: PropTypes.bool,
    turbo: PropTypes.bool
};

Controls.defaultProps = {
    active: false,
    turbo: false
};

export default injectIntl(Controls);
