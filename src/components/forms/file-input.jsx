import React from 'react';
import styles from './file-input.css';

class FileInput extends React.Component {
    constructor (props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.state = {
            file: null,
        };
    }

    onChange (e) {
        var files = e.target.files;
        if (files.length !== 0) {
            this.setState({ file: files[0] });
            // Forward the event to the parent
            this.props.onChange(e);
        }
    }

    render () {
        return (
            <div>
                <label className={styles.inputBrowseFilesButton}>
                    <input
                        type="file"
                        onChange={this.onChange}
                        // We hide the default input rendering which this
                        // component aims to replace.
                        style={{display: 'none'}}
                    />
                    Browse files...
                </label>
                {
                    // Only display the file name if on is selected
                    this.state.file && (
                        <span>
                            <span className={styles.selectedFileLabel}>
                                selected file:
                            </span>
                            <span className={styles.selectedFileName}>
                                {this.state.file.name}
                            </span>
                        </span>
                    )
                }
            </div>
        );
    }
}

export default FileInput;
