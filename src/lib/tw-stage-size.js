import paintStageSize from 'scratch-paint/src/lib/tw-stage-size';

import adacraft from '../adacraft'

const PARAM = 'size';

const getDimensions = () => {
    const defaultDimensions = adacraft.features.config.defaultStageDimensions || {
        width: 480,
        height: 270
    };

    // Running in node.js
    if (typeof URLSearchParams === 'undefined') {
        return defaultDimensions;
    }

    const urlParameters = new URLSearchParams(location.search);
    const dimensionsQuery = urlParameters.get(PARAM);
    if (dimensionsQuery === null) {
        return defaultDimensions;
    }

    const match = dimensionsQuery.match(/^(\d+)[^\d]+(\d+)$/);
    if (!match) {
        // eslint-disable-next-line no-alert
        alert('Could not parse custom stage size');
        return defaultDimensions;
    }
    const [_, widthText, heightText] = match;
    if (!widthText || !heightText) {
        return defaultDimensions;
    }

    const width = Math.max(0, Math.min(4096, +widthText));
    const height = Math.max(0, Math.min(4096, +heightText));

    return {
        width,
        height
    };
};

const dimensions = getDimensions();

paintStageSize.width = dimensions.width;
paintStageSize.height = dimensions.height;

export default dimensions;
