import React from 'react';
import {FormattedMessage} from 'react-intl';

import musicIconURL from './music/music.png';
import musicInsetIconURL from './music/music-small.svg';

import penIconURL from './pen/pen.png';
import penInsetIconURL from './pen/pen-small.svg';

import videoSensingIconURL from './videoSensing/video-sensing.png';
import videoSensingInsetIconURL from './videoSensing/video-sensing-small.svg';

import text2speechIconURL from './text2speech/text2speech.png';
import text2speechInsetIconURL from './text2speech/text2speech-small.svg';

import translateIconURL from './translate/translate.png';
import translateInsetIconURL from './translate/translate-small.png';

import makeymakeyIconURL from './makeymakey/makeymakey.png';
import makeymakeyInsetIconURL from './makeymakey/makeymakey-small.svg';

import microbitIconURL from './microbit/microbit.png';
import microbitInsetIconURL from './microbit/microbit-small.svg';
import microbitConnectionIconURL from './microbit/microbit-illustration.svg';
import microbitConnectionSmallIconURL from './microbit/microbit-small.svg';

import ev3IconURL from './ev3/ev3.png';
import ev3InsetIconURL from './ev3/ev3-small.svg';
import ev3ConnectionIconURL from './ev3/ev3-hub-illustration.svg';
import ev3ConnectionSmallIconURL from './ev3/ev3-small.svg';

import wedo2IconURL from './wedo2/wedo.png'; // TODO: Rename file names to match variable/prop names?
import wedo2InsetIconURL from './wedo2/wedo-small.svg';
import wedo2ConnectionIconURL from './wedo2/wedo-illustration.svg';
import wedo2ConnectionSmallIconURL from './wedo2/wedo-small.svg';
import wedo2ConnectionTipIconURL from './wedo2/wedo-button-illustration.svg';

import boostIconURL from './boost/boost.png';
import boostInsetIconURL from './boost/boost-small.svg';
import boostConnectionIconURL from './boost/boost-illustration.svg';
import boostConnectionSmallIconURL from './boost/boost-small.svg';
import boostConnectionTipIconURL from './boost/boost-button-illustration.svg';

import gdxforIconURL from './gdxfor/gdxfor.png';
import gdxforInsetIconURL from './gdxfor/gdxfor-small.svg';
import gdxforConnectionIconURL from './gdxfor/gdxfor-illustration.svg';
import gdxforConnectionSmallIconURL from './gdxfor/gdxfor-small.svg';

import twIcon from './tw/tw.svg';

import cloudlinkIconURL from './cloudlink/cloudlink.png';
import cloudlinkInsetIconURL from './cloudlink/cloudlink-inset.png';

import adavisionIconURL from './adavision/adavision.png';
import adavisionInsetIconURL from './adavision/adavision-icon-white.svg';

import adasoundIconURL from './adasound/IAsoncover.png';
import adasoundInsetIconURL from './adasound/IAsonpreview.png';

import adahttpIconURL from './adahttp/adahttp.png';
import adahttpInsetIconURL from './adahttp/adahttp-icon.svg';

import adabrowserIconURL from './adabrowser/ada.png';
import adabrowserInsetIconURL from './adabrowser/ada-icon.svg';

import adagifIconURL from './adagif/adagif.png';
import adagifInsetIconURL from './adagif/ada-icon.svg';

import adaruntimeIconURL from './adaruntime/ada.png';
import adaruntimeInsetIconURL from './adaruntime/ada-icon.svg';

import adap5IconURL from './adap5/P5JS.png';
import adap5InsetIconURL from './adap5/P5JS-icon.svg';

import adaleafletIconURL from './adaleaflet/adaleaflet.png';
import adaleafletInsetIconURL from './adaleaflet/ada-icon.svg';

import croquetIconURL from './croquet/croquet-background.png';
import croquetInsetIconURL from './croquet/croquet-icon.svg';

import posenet2scratchIconURL from './posenet2scratch/posenet2scratch.png';

import ml2scratchIconURL from './ml2scratch/ml2scratch.png';

import tm2scratchIconURL from './tm2scratch/tm2scratch.png';
import tm2scratchInsetIconURL from './tm2scratch/tm2scratch-small.png';

import customExtensionIcon from './custom/custom.svg';

import vittamicrobitIconURL from './vittamicrobit/microbit-cover.png';
import vittamicrobitInsetIconURL from './vittamicrobit/microbit-icon-white.svg';
import vittamicrobitIconGreenSmallURL from './vittamicrobit/microbit-icon-green-small.svg';

import vittaSTM32IconURL from './vittaSTM32/STM32-cover.png';
import vittaSTM32InsetIconURL from './vittaSTM32/STM32-icon-white.svg';
import vittaSTM32IconGreenSmallURL from './vittaSTM32/STM32-icon-green-small.svg';

import vittaarduinoIconURL from './vittaarduino/arduino-cover.png';
import vittaarduinoInsetIconURL from './vittaarduino/arduino-icon-white.svg';
import vittaarduinoIconGreenSmallURL from './vittaarduino/arduino-icon-green-small.svg';

import vittaESP32IconURL from './vittaESP32/ESP32-cover.png';
import vittaESP32InsetIconURL from './vittaESP32/ESP32-icon-white.svg';
import vittaESP32IconGreenSmallURL from './vittaESP32/ESP32-icon-green-small.svg';

import SPtiktokTTSIconSmallURL from './tiktokTTS/tiktokTTS-small.svg';
import SPtiktokTTSIconURL from './tiktokTTS/tiktokTTS.png';

import adacraft from '../../../adacraft'
const extensionsConfig = adacraft.features.config.extensions;

export default [
    {
        name: (
            <FormattedMessage
                defaultMessage="Arduino with USB"
                description="Name for the 'Vitta arduino' extension"
                id="gui.extension.vittaarduino.name"
            />
        ),
        tags: ['hardware'],
        extensionId: 'vittaarduino',
        collaborator: 'Vittascience',
        iconURL: vittaarduinoIconURL,
        insetIconURL: vittaarduinoInsetIconURL,
        projectLink: "https://vittascience.com/arduino/?link=620d8d83d526a&mode=code",
        description: (
            <FormattedMessage
                defaultMessage="Blocks to interact with arduino through a USB cable connection"
                description="Description for the 'Vitta arduino' extension"
                id="gui.extension.vittaarduino.description"
            />
        ),
        featured: true,
        disabled: true,
        usbConnectionRequired: true,
        launchSerialConnectionFlow: true,
        useAutoscan: false,
        connectionSmallIconURL: vittaarduinoIconGreenSmallURL
    },
    {
        name: (
            <FormattedMessage
                defaultMessage="micro:bit with USB"
                description="Name for the 'Vitta micro:bit' extension"
                id="gui.extension.vittamicrobit.name"
            />
        ),
        tags: ['hardware'],
        extensionId: 'vittamicrobit',
        collaborator: 'Vittascience',
        iconURL: vittamicrobitIconURL,
        insetIconURL: vittamicrobitInsetIconURL,
        projectLink: "https://vittascience.com/microbit/?link=623c4582a2510&mode=code",
        description: (
            <FormattedMessage
                defaultMessage="Blocks to interact with micro:bit through a USB cable connection"
                description="Description for the 'Vitta micro:bit' extension"
                id="gui.extension.vittamicrobit.description"
            />
        ),
        featured: true,
        disabled: false,
        usbConnectionRequired: true,
        launchSerialConnectionFlow: true,
        useAutoscan: false,
        connectionSmallIconURL: vittamicrobitIconGreenSmallURL
    },
    {
        name: (
            <FormattedMessage
                defaultMessage="STM32 with USB"
                description="Name for the 'Vitta STM32' extension"
                id="gui.extension.vittaSTM32.name"
            />
        ),
        tags: ['hardware'],
        extensionId: 'vittaSTM32',
        collaborator: 'Vittascience',
        iconURL: vittaSTM32IconURL,
        insetIconURL: vittaSTM32InsetIconURL,
        projectLink: "https://vittascience.com/stm32/?link=61c192415084f&mode=code",
        description: (
            <FormattedMessage
                defaultMessage="Blocks to interact with STM32 through a USB cable connection"
                description="Description for the 'Vitta STM32' extension"
                id="gui.extension.vittaSTM32.description"
            />
        ),
        featured: true,
        disabled: true,
        usbConnectionRequired: true,
        launchSerialConnectionFlow: true,
        useAutoscan: false,
        connectionSmallIconURL: vittaSTM32IconGreenSmallURL,
    },
    {
        name: (
            <FormattedMessage
                defaultMessage="ESP32 with USB"
                description="Name for the 'Vitta ESP32' extension"
                id="gui.extension.vittaESP32.name"
            />
        ),
        tags: ['hardware'],
        extensionId: 'vittaESP32',
        collaborator: 'Vittascience',
        iconURL: vittaESP32IconURL,
        insetIconURL: vittaESP32InsetIconURL,
        projectLink: "https://vittascience.com/esp32/?link=624ae3f864eb7&mode=code",
        description: (
            <FormattedMessage
                defaultMessage="Blocks to interact with ESP32 through a USB cable connection"
                description="Description for the 'Vitta ESP32' extension"
                id="gui.extension.vittaESP32.description"
            />
        ),
        featured: true,
        disabled: true,
        usbConnectionRequired: true,
        launchSerialConnectionFlow: true,
        useAutoscan: false,
        connectionSmallIconURL: vittaESP32IconGreenSmallURL,
    },
    {
        name: (
            <FormattedMessage
                defaultMessage="HTTP"
                description="Name for the 'Ada HTTP' extension"
                id="gui.extension.adahttp.name"
            />
        ),
        tags: ['network'],
        extensionId: 'adahttp',
        iconURL: adahttpIconURL,
        insetIconURL: adahttpInsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Some new blocks to send HTTP requests and manage results"
                description="Description for the 'Ada HTTP' extension"
                id="gui.extension.adahttp.description"
            />
        ),
        featured: true,
        internetConnectionRequired: true
    },
    {
        name: (
            <FormattedMessage
                defaultMessage="Browser"
                description="Name for the 'Ada browser' extension"
                id="gui.extension.adabrowser.name"
            />
        ),
        tags: ['others'],
        extensionId: 'adabrowser',
        iconURL: adabrowserIconURL,
        insetIconURL: adabrowserInsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Some new blocks to interact with the browser"
                description="Description for the 'Ada Browser' extension"
                id="gui.extension.adabrowser.description"
            />
        ),
        featured: true,
        internetConnectionRequired: true
    },
    {
        name: (
            <FormattedMessage
                defaultMessage="Gif"
                description="Name for the 'Ada gif' extension"
                id="gui.extension.adagif.name"
            />
        ),
        tags: ['others'],
        extensionId: 'adagif',
        iconURL: adagifIconURL,
        insetIconURL: adagifInsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Some new blocks to encode GIF"
                description="Description for the 'Ada Gif' extension"
                id="gui.extension.adagif.description"
            />
        ),
        featured: true,
        internetConnectionRequired: true
    },
    {
        name: (
            <FormattedMessage
                defaultMessage="adacraft runtime"
                description="Name for the 'Adacraft runtime' extension"
                id="gui.extension.adaruntime.name"
            />
        ),
        tags: ['others'],
        extensionId: 'adaruntime',
        iconURL: adaruntimeIconURL,
        insetIconURL: adaruntimeInsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Some new blocks to interact with the adacraft runtime (renderer, VM...)"
                description="Description for the 'Adacraft Runtime' extension"
                id="gui.extension.adaruntime.description"
            />
        ),
        featured: true,
        internetConnectionRequired: true
    },
    {
        name: (
            <FormattedMessage
                defaultMessage="p5.js"
                description="Name for the 'Adacraft p5.js' extension"
                id="gui.extension.adap5.name"
            />
        ),
        tags: ['art'],
        extensionId: 'adap5',
        iconURL: adap5IconURL,
        insetIconURL: adap5InsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Some blocks that use the p5.js library"
                description="Description for the 'Adacraft p5.js' extension"
                id="gui.extension.adap5.description"
            />
        ),
        featured: true,
        internetConnectionRequired: true
    },
    {
        name: (
            <FormattedMessage
                defaultMessage="Leaflet"
                description="Name for the 'Leaflet' extension"
                id="gui.extension.adaleaflet.name"
            />
        ),
        tags: ['others'],
        extensionId: 'adaleaflet',
        iconURL: adaleafletIconURL,
        insetIconURL: adaleafletInsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Blocks to display maps with Leaflet"
                description="Description for the 'Leaflet' extension"
                id="gui.extension.adaleaflet.description"
            />
        ),
        featured: true,
        internetConnectionRequired: true
    },
    {
        name: (
            <FormattedMessage
                defaultMessage="TikTok Text to Speech"
                description="Name for the 'Leaflet' extension"
                id="gui.extension.tiktoktts.name"
            />
        ),
        tags: ['others'],
        extensionId: 'tiktoktts',
        collaborator: 'SharkPool',
        iconURL: SPtiktokTTSIconURL,
        insetIconURL: SPtiktokTTSIconSmallURL,
        description: (
            <FormattedMessage
                defaultMessage="Text to Speech using TikTok API"
                description="Description for the 'Leaflet' extension"
                id="gui.extension.tiktoktts.description"
            />
        ),
        featured: true,
        internetConnectionRequired: true
    },
    {
        name: extensionsConfig?.adavision?.name || "Ada Vision",
        tags: ['ai'],
        extensionId: 'adavision',
        iconURL: adavisionIconURL,
        insetIconURL: adavisionInsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Use TeachableMachine models to detect things in images."
                description="Description for the 'Ada Vision' extension"
                id="gui.extension.adavision.description"
            />
        ),
        featured: true,
        internetConnectionRequired: true
    },
    {
        name: extensionsConfig?.adasound?.name || "Ada Sound",
        tags: ['ai'],
        extensionId: 'adasound',
        iconURL: adasoundIconURL,
        insetIconURL: adasoundInsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Use TeachableMachine models to detect things in sounds."
                description="Description for the 'Ada Sound' extension"
                id="gui.extension.adasound.description"
            />
        ),
        featured: true,
        internetConnectionRequired: true
    },
    {
        name: (
            <FormattedMessage
                defaultMessage="CloudLink"
                description="Name for the 'CloudLink' extension"
                id="gui.extension.cloudlink.name"
            />
        ),
        tags: ['network'],
        extensionId: 'cloudlink',
        collaborator: 'MikeDEV',
        iconURL: cloudlinkIconURL,
        insetIconURL: cloudlinkInsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="A powerful WebSocket extension."
                description="Description for the 'CloudLink' extension"
                id="gui.extension.cloudlink.description"
            />
        ),
        featured: true,
        internetConnectionRequired: true
    },
    {
        name: 'Croquet',
        tags: ['network'],
        extensionId: 'croquet',
        collaborator: 'Croquet Corporation',
        iconURL: croquetIconURL,
        insetIconURL: croquetInsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Croquet Collaboration Library."
                description="User Croquet to create collaborative features"
                id="gui.extension.croquet.description"
            />
        ),
        featured: true,
        disabled: false,
        internetConnectionRequired: true,
        useAutoScan: false,
    },
    {
        name: 'Posenet2Scratch',
        tags: ['ai'],
        extensionId: 'posenet2scratch',
        collaborator: 'champierre',
        iconURL: posenet2scratchIconURL,
        insetIconURL: posenet2scratchIconURL,
        description: (
            <FormattedMessage
                defaultMessage="PoseNet2Scratch Blocks."
                description="PoseNet2Scratch Blocks."
                id="gui.extension.posenet2scratch.description"
            />
        ),
        featured: true,
        disabled: false,
        internetConnectionRequired: true,
        useAutoScan: false,
    },
    {
        name: 'ML2Scratch',
        tags: ['ai'],
        extensionId: 'ml2scratch',
        collaborator: 'champierre',
        iconURL: ml2scratchIconURL,
        insetIconURL: ml2scratchIconURL,
        description: (
            <FormattedMessage
                defaultMessage="ML2Scratch Blocks."
                description="ML2Scratch Blocks."
                id="gui.extension.ml2scratch.description"
            />
        ),
        featured: true,
        disabled: false,
        internetConnectionRequired: true,
        useAutoScan: false,
    },
    {
        name: 'TM2Scratch',
        tags: ['ai'],
        extensionId: 'tm2scratch',
        collaborator: 'Tsukurusha, YengawaLab and Google',
        iconURL: tm2scratchIconURL,
        insetIconURL: tm2scratchInsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="画像や音声を学習させよう。"
                description="画像や音声を学習させよう。"
                id="gui.extension.tm2scratch.description"
            />
        ),
        featured: true,
        disabled: false,
        internetConnectionRequired: true,
        useAutoScan: false,
    },
    {
        name: (
            <FormattedMessage
                defaultMessage="Music"
                description="Name for the 'Music' extension"
                id="gui.extension.music.name"
            />
        ),
        extensionId: 'music',
        tags: ['scratch'],
        iconURL: musicIconURL,
        insetIconURL: musicInsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Play instruments and drums."
                description="Description for the 'Music' extension"
                id="gui.extension.music.description"
            />
        ),
        featured: true
    },
    {
        name: (
            <FormattedMessage
                defaultMessage="Pen"
                description="Name for the 'Pen' extension"
                id="gui.extension.pen.name"
            />
        ),
        extensionId: 'pen',
        tags: ['scratch'],
        iconURL: penIconURL,
        insetIconURL: penInsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Draw with your sprites."
                description="Description for the 'Pen' extension"
                id="gui.extension.pen.description"
            />
        ),
        featured: true
    },
    {
        name: (
            <FormattedMessage
                defaultMessage="Video Sensing"
                description="Name for the 'Video Sensing' extension"
                id="gui.extension.videosensing.name"
            />
        ),
        extensionId: 'videoSensing',
        tags: ['scratch'],
        iconURL: videoSensingIconURL,
        insetIconURL: videoSensingInsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Sense motion with the camera."
                description="Description for the 'Video Sensing' extension"
                id="gui.extension.videosensing.description"
            />
        ),
        featured: true
    },
    {
        name: (
            <FormattedMessage
                defaultMessage="Text to Speech"
                description="Name for the Text to Speech extension"
                id="gui.extension.text2speech.name"
            />
        ),
        extensionId: 'text2speech',
        tags: ['scratch'],
        collaborator: 'Amazon Web Services',
        iconURL: text2speechIconURL,
        insetIconURL: text2speechInsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Make your projects talk."
                description="Description for the Text to speech extension"
                id="gui.extension.text2speech.description"
            />
        ),
        featured: true,
        internetConnectionRequired: true
    },
    {
        name: (
            <FormattedMessage
                defaultMessage="Translate"
                description="Name for the Translate extension"
                id="gui.extension.translate.name"
            />
        ),
        extensionId: 'translate',
        tags: ['scratch'],
        collaborator: 'Google',
        iconURL: translateIconURL,
        insetIconURL: translateInsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Translate text into many languages."
                description="Description for the Translate extension"
                id="gui.extension.translate.description"
            />
        ),
        featured: true,
        internetConnectionRequired: true
    },
    {
        name: 'Makey Makey',
        extensionId: 'makeymakey',
        tags: ['scratch', 'hardware'],
        collaborator: 'JoyLabz',
        iconURL: makeymakeyIconURL,
        insetIconURL: makeymakeyInsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Make anything into a key."
                description="Description for the 'Makey Makey' extension"
                id="gui.extension.makeymakey.description"
            />
        ),
        featured: true
    },
    {
        name: 'micro:bit',
        extensionId: 'microbit',
        tags: ['scratch', 'hardware'],
        collaborator: 'micro:bit',
        iconURL: microbitIconURL,
        insetIconURL: microbitInsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Connect your projects with the world."
                description="Description for the 'micro:bit' extension"
                id="gui.extension.microbit.description"
            />
        ),
        featured: true,
        disabled: false,
        bluetoothRequired: true,
        internetConnectionRequired: true,
        launchPeripheralConnectionFlow: true,
        useAutoScan: false,
        connectionIconURL: microbitConnectionIconURL,
        connectionSmallIconURL: microbitConnectionSmallIconURL,
        connectingMessage: (
            <FormattedMessage
                defaultMessage="Connecting"
                description="Message to help people connect to their micro:bit."
                id="gui.extension.microbit.connectingMessage"
            />
        ),
        helpLink: 'https://scratch.mit.edu/microbit'
    },
    {
        name: 'LEGO MINDSTORMS EV3',
        extensionId: 'ev3',
        tags: ['scratch', 'hardware'],
        collaborator: 'LEGO',
        iconURL: ev3IconURL,
        insetIconURL: ev3InsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Build interactive robots and more."
                description="Description for the 'LEGO MINDSTORMS EV3' extension"
                id="gui.extension.ev3.description"
            />
        ),
        featured: true,
        disabled: false,
        bluetoothRequired: true,
        internetConnectionRequired: true,
        launchPeripheralConnectionFlow: true,
        useAutoScan: false,
        connectionIconURL: ev3ConnectionIconURL,
        connectionSmallIconURL: ev3ConnectionSmallIconURL,
        connectingMessage: (
            <FormattedMessage
                defaultMessage="Connecting. Make sure the pin on your EV3 is set to 1234."
                description="Message to help people connect to their EV3. Must note the PIN should be 1234."
                id="gui.extension.ev3.connectingMessage"
            />
        ),
        helpLink: 'https://scratch.mit.edu/ev3'
    },
    {
        name: 'LEGO BOOST',
        tags: ['scratch', 'hardware'],
        extensionId: 'boost',
        collaborator: 'LEGO',
        iconURL: boostIconURL,
        insetIconURL: boostInsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Bring robotic creations to life."
                description="Description for the 'LEGO BOOST' extension"
                id="gui.extension.boost.description"
            />
        ),
        featured: true,
        disabled: false,
        bluetoothRequired: true,
        internetConnectionRequired: true,
        launchPeripheralConnectionFlow: true,
        useAutoScan: true,
        connectionIconURL: boostConnectionIconURL,
        connectionSmallIconURL: boostConnectionSmallIconURL,
        connectionTipIconURL: boostConnectionTipIconURL,
        connectingMessage: (
            <FormattedMessage
                defaultMessage="Connecting"
                description="Message to help people connect to their BOOST."
                id="gui.extension.boost.connectingMessage"
            />
        ),
        helpLink: 'https://scratch.mit.edu/boost'
    },
    {
        name: 'LEGO Education WeDo 2.0',
        tags: ['scratch', 'hardware'],
        extensionId: 'wedo2',
        collaborator: 'LEGO',
        iconURL: wedo2IconURL,
        insetIconURL: wedo2InsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Build with motors and sensors."
                description="Description for the 'LEGO WeDo 2.0' extension"
                id="gui.extension.wedo2.description"
            />
        ),
        featured: true,
        disabled: false,
        bluetoothRequired: true,
        internetConnectionRequired: true,
        launchPeripheralConnectionFlow: true,
        useAutoScan: true,
        connectionIconURL: wedo2ConnectionIconURL,
        connectionSmallIconURL: wedo2ConnectionSmallIconURL,
        connectionTipIconURL: wedo2ConnectionTipIconURL,
        connectingMessage: (
            <FormattedMessage
                defaultMessage="Connecting"
                description="Message to help people connect to their WeDo."
                id="gui.extension.wedo2.connectingMessage"
            />
        ),
        helpLink: 'https://scratch.mit.edu/wedo'
    },
    {
        name: 'Go Direct Force & Acceleration',
        tags: ['scratch', 'hardware'],
        extensionId: 'gdxfor',
        collaborator: 'Vernier',
        iconURL: gdxforIconURL,
        insetIconURL: gdxforInsetIconURL,
        description: (
            <FormattedMessage
                defaultMessage="Sense push, pull, motion, and spin."
                description="Description for the Vernier Go Direct Force and Acceleration sensor extension"
                id="gui.extension.gdxfor.description"
            />
        ),
        featured: true,
        disabled: false,
        bluetoothRequired: true,
        internetConnectionRequired: true,
        launchPeripheralConnectionFlow: true,
        useAutoScan: false,
        connectionIconURL: gdxforConnectionIconURL,
        connectionSmallIconURL: gdxforConnectionSmallIconURL,
        connectingMessage: (
            <FormattedMessage
                defaultMessage="Connecting"
                description="Message to help people connect to their force and acceleration sensor."
                id="gui.extension.gdxfor.connectingMessage"
            />
        ),
        helpLink: 'https://scratch.mit.edu/vernier'
    },
    {
        name: (
            <FormattedMessage
                defaultMessage="TurboWarp Blocks"
                description="Name of TW extension"
                id="tw.twExtension.name"
            />
        ),
        extensionId: 'tw',
        tags: ['others'],
        iconURL: twIcon,
        description: (
            <FormattedMessage
                defaultMessage="Weird new blocks. Not compatible with Scratch."
                description="Description of TW extension"
                id="tw.twExtension.description"
            />
        ),
        featured: true,
        incompatibleWithScratch: true
    },
    {
        name: (
            <FormattedMessage
                defaultMessage="Custom Extension"
                description="Name of custom extension category"
                id="tw.customExtension.name"
            />
        ),
        extensionId: '',
        tags: ['others'],
        iconURL: customExtensionIcon,
        description: (
            <FormattedMessage
                defaultMessage="Load custom extensions from URLs. For developers. Experimental."
                description="Description of custom extension category"
                id="tw.customExtension.description"
            />
        ),
        featured: true
    },
];
